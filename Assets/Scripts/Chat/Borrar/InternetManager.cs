﻿using UnityEngine;
using System.Collections;

public class InternetManager :  Singleton<InternetManager> {

	public Canvas thisCanvas;
	public GameObject checkInternet;
	public GameObject loading;
	public GameObject blackMask;
	public GameObject pause;
	public GameObject npause;
	public GameObject nfocus;
	public GameObject focus;
	public bool hasInternet;
	public bool cheking;
	WWW  getVotosTotalCamisetas;

	void OnLevelWasLoaded(int level){
		// print("----------------------------------->>>>>>");
		thisCanvas.worldCamera = Camera.main;
	}

	public IEnumerator CheckInternet() {
		if(!cheking){
			// print("CHEQUEANDO!");
			loading.SetActive(true);
			cheking = true;
			getVotosTotalCamisetas = new WWW ("http://2waysports.com/2waysports/Ecuador/Barcelona/Camiseta/getVotosTotalCamisetas.php");
			yield return getVotosTotalCamisetas;
			hasInternet = !string.IsNullOrEmpty(getVotosTotalCamisetas.text);
			checkInternet.SetActive(!hasInternet);
			loading.SetActive(false);
			cheking = false;
		}
	}


}
