﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanelUsersNivelClub : MonoBehaviour {

	public static PanelUsersNivelClub MyMang;
	string urlDownloadActives = "http://roguez.us/2WAYSPORTS.COM/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/DownloadUserNivelClub.php?";
	public CardViewUserNC cardViewUSER;
	public GameObject [] UserNC;
	public Text [] Letters;
	 public GameObject User_NivelClub;
	 public GameObject Padre_User;
	int actualpos, afterpos;
	 public Text Nofound;
	 public Text letterSelected;
	public float minSwipeDistY;
	public float minSwipeDistX;
	private Vector2 startPos;
	// Use this for initialization
	void Start () {
		MyMang = this;
		CardViewUserNC.CardView = cardViewUSER;
		StartCoroutine( DownloadInfo( ));
	}

	void Update(){
		#if UNITY_ANDROID || UNITY_IOS
		SwipeChangePos( );
		#endif
	}

	public void Download_InfoUsers(){
		Nofound.text = "";
		ViewAllUser( );
	}
		
	public void sizeLetter( bool state){
		int initSize = 40;
		float PosX = 2.482847f;
		for(int i =0 ; i < Letters.Length; i++){
			Letters[i].fontSize = initSize;
			Letters[i].rectTransform.position  = new Vector3(PosX,Letters[i].rectTransform.position.y,Letters[i].rectTransform.position.z);
		}
		letterSelected.fontSize = (state) ? 100 : initSize;
		PosX = (state) ? PosX+1f :  PosX;
		letterSelected.rectTransform.position = new Vector3(PosX+1f,letterSelected.rectTransform.position.y,letterSelected.rectTransform.position.z);
	}

	public IEnumerator DownloadInfo ( ){
		InternetManager.main.loading.SetActive(true);

		ClearUsersView();
		int users = 0;
		string constr = urlDownloadActives + "opc=LoadActives";
		WWW consult = new WWW(constr);
		yield return consult;
		string consult_temp = consult.text;
		if( !string.IsNullOrEmpty(consult_temp) ){
			string temp = null;
			for(int i =0; i < consult_temp.Length ;i++){
				if(consult_temp[i] != '[' && consult_temp[i] != ']'){
					temp = temp + consult_temp[i];	
				}else{
					if(consult_temp[i] == ']'){ 	
						if(temp != DataApp.main.GetMyID().ToString()){
							UserNC = new GameObject[UserNC.Length+1];
							GameObject newUser = Instantiate( User_NivelClub, Padre_User.transform.position, Quaternion.identity) as GameObject;
							newUser.name = users.ToString( );
							newUser.transform.SetParent(Padre_User.transform, false);
							newUser.GetComponent<UsuarioNivelClub>().Id = temp;
							users++;
							yield return new WaitForEndOfFrame ( );
						}
					}
					temp = null;
				}
			}
			for(int i = 0; i<UserNC.Length;i++){
				UserNC[i] = Padre_User.transform.GetChild(i).gameObject;
			}
		}else{
			ProfileUser.MyUser.EnablePopUp("Fallo en la conexión a internet. \nIntentelo de nuevo.");
			StartCoroutine(DownloadInfo());
		}
	}


	public void SwipeChangePos(){
		if (Input.touchCount > 0)  {
			Touch touch = Input.touches[0];
			switch (touch.phase) 	{
			case TouchPhase.Began:
				startPos = touch.position;
				break;
			case TouchPhase.Ended:
				float swipeDistHorizontal = (new Vector3(touch.position.x,0, 0) - new Vector3(startPos.x, 0, 0)).magnitude;  /// Moviemiento lateral Swipe
				if (swipeDistHorizontal > minSwipeDistX) 	{
					float swipeValue = Mathf.Sign(touch.position.x - startPos.x);
					if (swipeValue > 0) {       //right swipe
							ChangeViewUser(-1);
					}else if (swipeValue < 0) {     //left swipe
							ChangeViewUser(1);
					}
				}
				break;
			}
		}
	}

	void ClearUsersView(){
		
		for(int i = 0; i<UserNC.Length;i++){
			Destroy(UserNC[i]);
		}
		UserNC = new GameObject[0];
	}

	public void ChangeViewUser ( int index){
		actualpos = CardViewUserNC.CardView.indexUser;
		actualpos += index;
		if( actualpos >= 0 && actualpos <= UserNC.Length-1){
			afterpos = actualpos;
			UserNC[actualpos].GetComponent<UsuarioNivelClub>().sendCardView();
		}else{
			actualpos = afterpos;
		}
	}

	public void SearchsUser(InputField inputSearch ){
		int actives = 0;
		string input = inputSearch.text.ToUpper();
		string nicknameCompare = null;
		string emailCompare = null;
		ViewAllUser( );
		foreach ( GameObject user in UserNC){
			nicknameCompare = user.GetComponent<UsuarioNivelClub>( ).Datas[1].text.ToUpper();
			emailCompare = user.GetComponent<UsuarioNivelClub>( ).Datas[3].text.ToUpper();
			Debug.Log(" email: "+  emailCompare);
			if(nicknameCompare.Contains(input)){
				user.gameObject.SetActive(true);
				actives++;
			}else if (emailCompare.Contains(input)){
				user.gameObject.SetActive(true);
				actives++;
			}else{
				user.gameObject.SetActive(false);
			}
		}
		if(actives<=0)
			Nofound.text = "No se han encontrado resultados de su busqueda.";
		else
			Nofound.text = "";
		InternetManager.main.loading.SetActive(false);

	}


	public void OpenCardView( ){
		cardViewUSER.gameObject.SetActive(true);
	}

	public void ViewAllUser( ){
		Nofound.text = "";
		foreach ( GameObject user in UserNC){
				user.gameObject.SetActive(true);
		}
		for(int i =0 ; i < Letters.Length; i++){
			Letters[i].fontSize = 40;
		}
	}

	public void SearchsUserAlphab( Text alphab ){
		int actives = 0;
		letterSelected = alphab;
		// string input = alphab.text.ToUpper();
		string dataCompare = null;
		foreach ( GameObject user in UserNC){
			dataCompare = user.GetComponent<UsuarioNivelClub>().Datas[1].text.ToUpper();
			if(dataCompare.StartsWith(alphab.text)){
				user.gameObject.SetActive(true); 
				actives++;
			}else{
				user.gameObject.SetActive(false);
			}
		}
		if(actives<=0)
			Nofound.text = "No se han encontrado resultados de su busqueda.";
		else
			Nofound.text = "";

		InternetManager.main.loading.SetActive(false);

	}
}
