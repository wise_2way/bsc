﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Events;


public class ToggleComplement : MonoBehaviour {
	public UnityEvent onValueTurnOn;
	public UnityEvent onValueTurnOff;
	Toggle mToggle;
	Text[] mText;
	public Color[] colors;

	void Awake () {
		mToggle = GetComponent(typeof(Toggle) ) as Toggle;
		mText = mToggle.GetComponentsInChildren<Text>();
		mToggle.onValueChanged.AddListener(Changed);
		TasksFromThisProject();
	}

	public void DisableEventsTurnOn(){ //CAMBIAR NOMBRE
   		for(int i=0; i<onValueTurnOn.GetPersistentEventCount() ; i++){
   			print("Cambiado "+i+" a estado >>>> off");
			onValueTurnOn.SetPersistentListenerState(i,UnityEventCallState.Off);
   		}
	}

	public void EnableEventsTurnOn(){ //CAMBIAR NOMBRE
   		for(int i=0; i<onValueTurnOn.GetPersistentEventCount() ; i++){
   			print("Cambiado "+i+" a estado >>>> on");
			onValueTurnOn.SetPersistentListenerState(i,UnityEventCallState.EditorAndRuntime);
   		}
	}
		
	void TasksFromThisProject(){
		ToggleComplement mToggleComplement = GetComponent(typeof(ToggleComplement) ) as ToggleComplement;
		onValueTurnOn.AddListener ( delegate {mToggleComplement.TextColor(0); });
		onValueTurnOff.AddListener( delegate {mToggleComplement.TextColor(1); });
	}

	void Changed (bool _s) {
		if(_s){
			onValueTurnOn.Invoke();
		}else{
			onValueTurnOff.Invoke();
		}
	}

	public void TextColor(int _index){
		foreach (Text t in mText) {
			if(t.tag == "Inverse"){
				t.color = colors[_index==1?0:1]; 
			}else{
				t.color = colors[_index]; 
			}
		}
	}

}
