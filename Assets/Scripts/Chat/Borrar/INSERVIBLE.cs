﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class INSERVIBLE : MonoBehaviour {
	public Button b;
	public Button bCreate;
	public InputField inf;
	public ConfirmationPopUp cp;

	// Use this for initialization
	void Start () {
		inf.text= PlayerPrefs.GetString("pincheID");
		bCreate.onClick.AddListener(()  => {ChatController.main.CreateUserDUMMY(inf.text);});
		// bCreate.onClick.Invoke();
		// b.onClick.Invoke();
	}

	public void DeleteAll(){
		PlayerPrefs.DeleteAll();
	}


	public void methodButton(){
		PlayerPrefs.SetString("pincheID",inf.text);

	}
}
