﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.ComponentModel.Design;

public class CardViewUserNC : MonoBehaviour {

	public static CardViewUserNC CardView;
	private string padrinoDownload = "http://roguez.us/2WAYSPORTS.COM/2waysports/Ecuador/Barcelona/publicidad/oro";
	public Image photProfile_ac;
	[HideInInspector] public Text[] DataCardView;
	public Button Back, Next;
	//  Publicidad
	public bool activedPublic;
	[HideInInspector] public Image padrinoOro1, padrinoOro2;
	public int indexUser;
	public string idUser;

	void Start(){
		CardView = this;
		if(activedPublic){
			StartCoroutine(DownloadPadrino("1",padrinoOro1));
			StartCoroutine(DownloadPadrino("2",padrinoOro2));
		}
	}


		
	public void SetDatas (string _id, int actualuser, string nickname, string fecha_ac, string scoreTa, string posTa, string scorePronos, string posPronos, string desc, Image photoProfile){
		idUser =_id;

		CardView.gameObject.SetActive(true);
		DataCardView[0].text = nickname;
		DataCardView[1].text = "DESDE "+fecha_ac;
		DataCardView[2].text = scoreTa;
		DataCardView[3].text = posTa;
		DataCardView[4].text = scorePronos;
		DataCardView[5].text = posPronos;
		DataCardView[6].text = desc;
		photProfile_ac.sprite = photoProfile.sprite;
		InternetManager.main.loading.SetActive(false);

		if(indexUser == 0){
			Back.interactable = false;
			Next.interactable = true;
		}else if (indexUser == PanelUsersNivelClub.MyMang.UserNC.Length-1){
			Back.interactable = true;
			Next.interactable = false;
		}else{
			Back.interactable = true;
			Next.interactable = true;
		}
	}

	IEnumerator DownloadPadrino( string index, Image img){
		string constr = padrinoDownload+index+".jpg";
		WWW consult = new WWW(constr);
		yield return consult;
		if( string.IsNullOrEmpty(consult.error)){
			Rect dim = new Rect(0,0,consult.texture.width,consult.texture.height);
			img.sprite = Sprite.Create(consult.texture,dim, Vector2.zero);
		}else{
			ProfileUser.MyUser.EnablePopUp("Fallo en la conexión.\nIntentelo de nuevo.");
		}
	}
}
