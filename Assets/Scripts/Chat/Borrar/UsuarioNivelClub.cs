﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Security.Cryptography;

public class UsuarioNivelClub : MonoBehaviour {

	public static UsuarioNivelClub userNC;
	string urlDownloadActives = "http://roguez.us/2WAYSPORTS.COM/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/DownloadUserNivelClub.php?";
	string ImageProfile = "http://roguez.us/2WAYSPORTS.COM/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/";
	private string getUrlScoreTiroAmarillo = "http://2waysports.com/2waysports/Ecuador/Barcelona/rankingTiroAmarillo/MostrarPuntajeXUsuario.php";
	private string getUrlPosTiroAmarillo = "http://2waysports.com/2waysports/Ecuador/Barcelona/rankingTiroAmarillo/MostrarPosXUsuario.php";
	private string getUrlScorePronostico = "http://2waysports.com/2waysports/Ecuador/Barcelona/Pronostico/Php/MostrarPuntajeXUsuario.php";
	private string getUrlPosPronostico = "http://2waysports.com/2waysports/Ecuador/Barcelona/Pronostico/Php/MostrarPosXUsuario.php";
	public string Id;
	public int cantidad_Datos_Pedidos = 5;
	public Image profileImage;
	public Text [] Datas;
	public string PosTAmarillo, ScoreTAmarillo;
	public string PosPronostico, ScorePronostico;

	void Start ( ) {
		userNC = GetComponent<UsuarioNivelClub> ( );
		StartCoroutine(DownloadDataUser(Id));
		StartCoroutine(DownloadDataTiroAmarillo(Id));
		StartCoroutine(DownloadDataPronostico(Id));
	}

	public void sendCardView ( ) {
		InternetManager.main.loading.SetActive(true);
		CardViewUserNC.CardView.indexUser = int.Parse(this.name);
		int indexSelect = CardViewUserNC.CardView.indexUser;
		CardViewUserNC.CardView.SetDatas( Id,indexSelect, Datas[1].text,Datas[4].text,ScoreTAmarillo,PosTAmarillo,ScorePronostico,PosPronostico,Datas[2].text, profileImage);
	}

	IEnumerator DownloadImageProfile ( ){
		InternetManager.main.loading.SetActive(true);
		string urlimg = ImageProfile+Id+".jpg";
		WWW d_img = new WWW(urlimg);
		yield return d_img;
		InternetManager.main.loading.SetActive(false);
		if(string.IsNullOrEmpty(d_img.error) && d_img.bytes.Length > 1699){
			Rect dim = new Rect(0,0,d_img.texture.width,d_img.texture.height);
			profileImage.sprite = Sprite.Create(d_img.texture,dim, Vector2.zero);
			float alpha = 255f;
			profileImage.color = new Color(255,255,255, alpha);
		}else{
			float black = 0f;
			profileImage.color = new Color(profileImage.color.r, profileImage.color.g, profileImage.color.b, 225);
		}
		
	}

	IEnumerator DownloadDataTiroAmarillo (string id ){
		//PUNTAJE TIRO AMARILLO
		string scoreTa = getUrlScoreTiroAmarillo+"?userID="+id;
		WWW scoreTa_c = new WWW(scoreTa);
		yield return scoreTa_c;
		if( !string.IsNullOrEmpty(scoreTa_c.text)){
			ScoreTAmarillo = scoreTa_c.text.Trim();
		}
		//POSICION TIRO AMARILLO
		string posTa = getUrlPosTiroAmarillo+"?userID="+id;
		WWW posTa_c = new WWW(posTa);
		yield return posTa_c;
		if( !string.IsNullOrEmpty(posTa_c.text)){
			PosTAmarillo = posTa_c.text.Trim();
		}
	}

	IEnumerator DownloadDataPronostico (string id ){
		//PUNTAJE PRONOSTICO
		string scoreTa = getUrlScorePronostico+"?userID="+id;
		WWW scoreTa_c = new WWW(scoreTa);
		yield return scoreTa_c;
		if( !string.IsNullOrEmpty(scoreTa_c.text)){
			ScorePronostico = scoreTa_c.text.Trim();
		}
		//POSICION PRONOSTICO
		string posTa = getUrlPosPronostico+"?userID="+id;
		WWW posTa_c = new WWW(posTa);
		yield return posTa_c;
		if( !string.IsNullOrEmpty(posTa_c.text)){
			PosPronostico = posTa_c.text.Trim();
		}
	}

	IEnumerator DownloadDataUser(string id) {
		InternetManager.main.loading.SetActive(true);
		int count =0;
		string c = urlDownloadActives + "opc=LoadData&userID="+id;
		WWW consult = new WWW(c);
		yield return consult;
		if( !string.IsNullOrEmpty(consult.text)){
			StartCoroutine(DownloadImageProfile());
			string data = consult.text;
			string temp= null;
			for(int i = 0; i < data.Length; i++){
				if(count < cantidad_Datos_Pedidos){
					if(data[i] != '[' && data[i] != ']'){
						temp = temp+ data[i];
					}else{
						if(data[i] == ']'){ 
							Datas[count].text = temp;
							count++;
							temp=null;
						}
					}
				}
			}
			InternetManager.main.loading.SetActive(false);
		}
	}
}
