﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ConfirmationPopUp : MonoBehaviour {
	public Button okButton;
	public Button cancelButton;
	public Text message;
	bool userChoose;
	string result;


	void Start(){
		okButton.onClick.AddListener(()=> {
			userChoose =true;
			result ="ok";
		});
		cancelButton.onClick.AddListener(()=> {
			userChoose = true;
			result ="cancel";
		});
	}

	public IEnumerator AskToUser(System.Action<string> callback , string _msj){
		message.text = _msj;
		yield return new WaitUntil(() => userChoose);
		callback(result);
		userChoose = false;
		result = "";
	}

}
