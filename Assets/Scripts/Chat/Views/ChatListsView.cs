﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ChatListsView : MonoBehaviour,IChatView {
	public CardRoom  cardPrefab;
	public Button newChat;
	List<CardRoom> cards = new List<CardRoom>();

	// Use this for initialization
	public void SetupView () {
		this.gameObject.SetActive(true);
		RegisterListeners();
		ShowOwnRooms();
		print("Aca se ejecuta el borrado");
	}

	void ShowOwnRooms(){
		Parameter p1 = new Parameter("MemberId",ChatController.main.myUserIDchat);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);

		StartCoroutine(ApiChat.main.GET<ChatsMembersList>( chatmbs  => {
			PutOwnRooms(chatmbs);
		} ,PCI.CHATS_MEMBERS,parameters));
	}



	void PutOwnRooms(ChatsMembersList chatmbs){
		foreach (ChatsMembers cm in chatmbs.list) {
			if(cm.ChatId.ToString() !=  ApiChat.main.idMuroAmarillo){
				CheckSizeOfRoomAndInstantiate(cm.Chats);
			}
		}
	}


	void CheckSizeOfRoomAndInstantiate(Rooms _r){
		Parameter p1 = new Parameter("ChatId",_r.ChatId);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);

		StartCoroutine(ApiChat.main.GET<ChatsMembersList>( chatmbs  => {
			

			InstantiateCard(cardPrefab , _r, chatmbs.list.Count );


		} ,PCI.CHATS_MEMBERS,parameters));
	}



	void InstantiateCard(CardRoom _c , Rooms _cid, int size){
        CardRoom card = Instantiate(_c) ;
        card.transform.SetParent (_c.transform.parent, false);
        cards.Add(card);
        card.gameObject.SetActive(true);
        card.SetValues( _cid, size);
	}

	public void DisposeView(){
		foreach (CardRoom c in cards) {
			Destroy(c.gameObject,0.03f);
		}
		cards.Clear();
		this.gameObject.SetActive(false);
		newChat.onClick.RemoveAllListeners();
	}

	void RegisterListeners(){
		newChat.onClick.AddListener( ()=> { 
			ChatController.main.ActiveView(VIEW_CHAT.EDIT) ;
			New_EditChatView newView = (New_EditChatView)ChatController.main.actualView;
			newView.SetLikeNewView();
		} );
	}

}
