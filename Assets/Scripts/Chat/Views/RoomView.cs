using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.IO;
using System.Collections.Generic;

public class RoomView : MonoBehaviour,IChatView {
	public CardChat  cardLocalPrefab;
	public CardChat  cardRemotePrefab;

	//Eduardo
	public Text myUserName, senderUserName;
	//--------
	public MyInputField  inputF;
	public Button  buttonSend;
	public Text title;

	public Button btnBlocks;
	public Button btnInvites;
	public Button btnEdit;
	public Button btnExit;

	List<CardChat> cards = new List<CardChat>();
	List<Member> remoteMembers = new List<Member>();
	Rooms thisRomm;
	DirectoryInfo folder;

	// Use this for initialization
	public void SetupView () {
		
		thisRomm = ChatController.main.actualRoom;
		CreateDirectoryToHistory();
		this.gameObject.SetActive(true);
		RegisterListeners();
		// CLEARView();
		SetTitleRoom();
		GetRemoteMembers();

	}

	void GetRemoteMembers(){
		Parameter p1 = new Parameter("ChatId",thisRomm.ChatId);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);
		ApiChat.main.StartCoroutine(ApiChat.main.GET<ChatsMembersList>( myChatsMembers => {
			if(myChatsMembers != null){
				foreach (ChatsMembers cm in myChatsMembers.list ) {
					remoteMembers.Add(cm.Members);
				}
				ShowPastMessages();
				InvokeRepeating ("ShowNewMessages",0f,2f);
			}
		} ,PCI.CHATS_MEMBERS,parameters ));
	}

	void CreateDirectoryToHistory(){
		folder = new DirectoryInfo(Application.persistentDataPath +"/"+thisRomm.ChatId);
		if(!folder.Exists){
			folder.Create();
			print(" LA CARPETA; "+folder.ToString());
		}
	}

	public void SetTitleRoom(){
		title.text = thisRomm.DisplayName;
		bool isMA = thisRomm.ChatId == ChatController.main.muroAmarillo.ChatId;
		btnEdit.gameObject.SetActive(!isMA);
		btnExit.gameObject.SetActive(!isMA);
	}

	void ShowNewMessages(){
		Parameter p1 = new Parameter("ChatId",thisRomm.ChatId);
		Parameter p2 = new Parameter("Receiver",ChatController.main.myUserIDchat);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);
		parameters.Add(p2);

		ApiChat.main.StartCoroutine(ApiChat.main.PUT<MessagesList>( msjs => {
			if(msjs != null){
				// CheckRemoteMembersAndThenPutMessage(msjs, true);
				PutMessages(msjs, true);
			}else{

			}
		} ,PCI.MESSAGES,parameters ));
	}
	
	void ShowPastMessages(){
		if(File.Exists(folder.ToString()+"/history.json")){
			string json = "{\"list\":["+File.ReadAllText(  folder.ToString()+"/history.json"  )+"]}";
			PutMessages(  JsonUtility.FromJson<MessagesList>(json), false );
		}else{
			File.WriteAllText(folder.ToString() +"/history.json", "{}" );
		}
	}

	// void CheckRemoteMembersAndThenPutMessage(MessagesList msjs, bool isNew){
	void CheckRemoteMembersAndThenInstantiateCard(Messages _msj){
		CardChat cc = _msj.Sender == ChatController.main.myUserIDchat ? cardLocalPrefab :cardRemotePrefab;
		string nameToShow = "";
		foreach (Member m in remoteMembers) {
			if(m.MemberId == _msj.Sender){
				nameToShow = m.DisplayName;
			}
		}
		if(string.IsNullOrEmpty (nameToShow) ){
			ApiChat.main.StartCoroutine(ApiChat.main.GETraw<MembersList>( users => {
				if(users == null){
					nameToShow ="Unknown User";
				}else{
					remoteMembers.Add( users.list[0]);
					nameToShow = users.list[0].DisplayName;
				}
				InstantiateCard(cc,_msj.Text,nameToShow );//GUARDAR

				InternetManager.main.loading.SetActive(true);
			} ,PCI.MEMBERS,_msj.Sender.ToString() ));
			
		}else{
			InstantiateCard(cc,_msj.Text,nameToShow );//GUARDAR
		}
	}


	void PutMessages(MessagesList msjs, bool isNewMessages){
		foreach (Messages m in msjs.list) {
			CheckRemoteMembersAndThenInstantiateCard(m);
			if(isNewMessages){
				File.AppendAllText(folder.ToString() +"/history.json", CreateObjectRemotoAndTRansformToJson(m) );
			}
		}
	}

	public void PutLocalMessageUI(){
		if(!string.IsNullOrEmpty(inputF.text) ){
			PutLocalMessageToServer(inputF.text);
		}else{ }
	}

	void PutLocalMessageToServer(string _text){
		Parameter p1 = new Parameter("SenderMemberId",ChatController.main.myUserIDchat);
		Parameter p2 = new Parameter("ChatId",ChatController.main.actualRoom.ChatId);
		Parameter p3 = new Parameter("text",System.Uri.EscapeUriString(_text));
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1); parameters.Add(p2); parameters.Add(p3);

		ApiChat.main.StartCoroutine(ApiChat.main.POST( result => {
			if(result == "\"Sucessfully\""){
				InstantiateCard(cardLocalPrefab,inputF.text,ChatController.main.me.DisplayName);
				File.AppendAllText(folder.ToString() +"/history.json", CreateObjectLocalAndTRansformToJson(inputF.text) );

        		inputF.text ="";
			}else{

			}
		} ,PCI.MESSAGES,parameters ));
	}

	void InstantiateCard(CardChat _c, string _msj, string _name){
		// print("$$$$$$$$$ llega este name"+_name);
		if(!string.IsNullOrEmpty(_msj))
		{
	        CardChat card = Instantiate(_c) ;
	        card.transform.SetParent (_c.transform.parent, false);
	        cards.Add(card);
	        card.gameObject.SetActive(true);
	        card.SetTextMessage(_msj);
	        card.userName.text = _name;//Eduardo afecta todos incluso los remotos con mi nombre de usuario
		}
	}


	string CreateObjectRemotoAndTRansformToJson(Messages _m){
		string result = "";
		MessagesCreator myMsj =  new MessagesCreator(_m.Receivers, _m.Sender, _m.Text, _m.IsFile );
		result = JsonUtility.ToJson( myMsj);
		return ","+result;
	}

	string CreateObjectLocalAndTRansformToJson(string _m){
		string result = "";
		MessagesCreator myMsj =  new MessagesCreator(new ChatsMembers(ChatController.main.actualRoom,ChatController.main.me) , ChatController.main.myUserIDchat , _m, false );
		result = JsonUtility.ToJson( myMsj);
		return ","+result;
	}

	// void CLEARView(){

	// }

	public void DisposeView(){
		foreach (CardChat c in cards) {
			Destroy(c.gameObject,0.03f);
		}
		cards.Clear();
		this.gameObject.SetActive(false);
		buttonSend.onClick.RemoveAllListeners();
		btnBlocks.onClick.RemoveAllListeners();
		btnInvites.onClick.RemoveAllListeners();
		btnEdit.onClick.RemoveAllListeners();
		btnExit.onClick.RemoveAllListeners();
		remoteMembers.Clear();
		CancelInvoke();
	}

	
	string NameOfThisID(int _id){
		string result = "";
		foreach (Member m in remoteMembers) {
			if(m.MemberId == _id){
				result = m.DisplayName;
			}
		}
		if(string.IsNullOrEmpty(result)){
			ApiChat.main.StartCoroutine(ApiChat.main.GETraw<MembersList>( users => {
				if(users == null){
					result ="Unknown User";
				}else{
					remoteMembers.Add( users.list[0]);
					result = users.list[0].DisplayName;
				}
				InternetManager.main.loading.SetActive(true);
			} ,PCI.MEMBERS,_id.ToString() ));
		}
		return result;
	}

	void DeleteMeFromThisRoom(){
		Parameter p1 = new Parameter("ChatId",thisRomm.ChatId);
		Parameter p2 = new Parameter("MemberId",ChatController.main.myUserIDchat);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);
		parameters.Add(p2);

		ApiChat.main.StartCoroutine(ApiChat.main.DELETE( deletes => {
			if( deletes ){
				ChatController.main.ActiveView(VIEW_CHAT.LISTS);
			}else{
			}
		} ,PCI.CHATS_MEMBERS,parameters ));
	}

	void RegisterListeners(){

		btnBlocks.onClick.AddListener(()=>{
			ChatController.main.ActiveView(VIEW_CHAT.BLOCKS);
		});
		
		btnInvites.onClick.AddListener(()=>{
			ChatController.main.ActiveView(VIEW_CHAT.INVITES);
		});

		btnEdit.onClick.AddListener(()=>{
			ChatController.main.ActiveView(VIEW_CHAT.EDIT);
			ChatController.main.SetLikeEdit(thisRomm );
				// EDITAR ESPECIFICO
		});

		btnExit.onClick.AddListener(()=>{
			ChatController.main.cp.gameObject.SetActive(true);
			ApiChat.main.StartCoroutine(ChatController.main.cp.AskToUser(result =>{
				if(result == "ok"){
					print ("ELIGIO OK");
					DeleteMeFromThisRoom();
				}else{
					print ("ELIGIO "+result);
				}	
				ChatController.main.cp.gameObject.SetActive(false);
			},"¿Desea salir del grupo "+thisRomm.DisplayName+"?" ));
		});

		buttonSend.onClick.AddListener( PutLocalMessageUI);
		// buttonSend.onClick.AddListener( inputF.Select);
		// InputField.SubmitEvent subEvent = new InputField.SubmitEvent();
		// subEvent.AddListener( PutLocalMessageUI);
		// inputF.onEndEdit = subEvent;
		// inputF.OnSubmit( (x) => {PutLocalMessageUI();} );
		// inputF.onSubmit.AddListener( (x) => PutLocalMessageUI() );
		// inputF.onEndEdit.AddListener( (x) => {PutLocalMessageUI();} );
		// inputF.onEndEdit.AddListener( (x) => {inputF.Select();} );
	}
}
