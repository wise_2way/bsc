using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class New_EditChatView : MonoBehaviour,IChatView {
	public CardMember  cardMemberPrefab;
	public CardMember  cardInvitedPrefab;
	public GameObject  separator;
	public Button editName;
	public Button setName;

	public Button buttonDelete;
	// public Button buttonCreate;
	public Button buttonInvite;
	public InputField nameField;
	public Text nameChat;
	public Text title;
	public Rooms room { get; private set;}

	List<CardMember> cards = new List<CardMember>();

	// Use this for initialization
	public void SetupView () {
		this.gameObject.SetActive(true);
		separator.SetActive(false);
		RegisterListeners();
	}

	public void SetLikeEditView(Rooms _r){
		room = _r;
		title.text ="EDITAR CHAT";
		nameChat.text = room.DisplayName;

		nameChat.gameObject.SetActive(true);
		nameField.gameObject.SetActive(false);
		buttonDelete.gameObject.SetActive(false);
		
		bool isOwn = room.Owner == ChatController.main.myUserIDchat  ;
		buttonInvite.gameObject.SetActive(isOwn);
		DecideShowDeleteButton(room);
		ShowMembersInRoom();
		ShowInvitesToRoom();
	}

	void DecideShowDeleteButton(Rooms _r){
		Parameter p1 = new Parameter("ChatId",_r.ChatId);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);
		StartCoroutine(ApiChat.main.GET<ChatsMembersList>( chatmbs  => {
			bool canDelete = chatmbs.list.Count < 2;
			buttonDelete.gameObject.SetActive(canDelete);
		} ,PCI.CHATS_MEMBERS,parameters));
	}
	

	public void SetLikeNewView(){
		nameChat.gameObject.SetActive(false);
		nameField.gameObject.SetActive(true);

		buttonDelete.gameObject.SetActive(false);
		// buttonCreate.gameObject.SetActive(false);
		buttonInvite.gameObject.SetActive(false);

		nameChat.text = "";
		title.text ="NUEVO CHAT";
	}

	void ShowMembersInRoom(){
		Parameter p1 = new Parameter("ChatId",room.ChatId);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);

		ApiChat.main.StartCoroutine(ApiChat.main.GET<ChatsMembersList>( chatmbs  => {
			PutMembersInRoom(chatmbs);
		} ,PCI.CHATS_MEMBERS,parameters));
	}


	void ShowInvitesToRoom(){
		Parameter p1 = new Parameter("ChatId",room.ChatId);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);

		ApiChat.main.StartCoroutine(ApiChat.main.GET<InvitationsList>( invites  => {
			// if(invites != nul){
			if(invites.list.Count > 0){
				separator.SetActive(true);
			}
			PutInvitesInRoom(invites);
		} ,PCI.INVITATIONS,parameters));
	}

	void RegisterListeners(){

		buttonDelete.onClick.AddListener( ()=>{
			ChatController.main.cp.gameObject.SetActive(true);
			ApiChat.main.StartCoroutine(ChatController.main.cp.AskToUser(result =>{
				if(result == "ok"){
					DeleteThisRoom();
					print ("ELIGIO OK");
				}else{
					print ("ELIGIO "+result);
				}	
				ChatController.main.cp.gameObject.SetActive(false);
			},"¿Desea eliminar el grupo "+nameChat.text+"?" ));
		} );

		buttonInvite.onClick.AddListener( ()=> { 
			ChatController.main.ActiveView(VIEW_CHAT.MEMBERS);
				// CreateNewChat();
		} );

		setName.onClick.AddListener( ()=> { 
			if(! string.IsNullOrEmpty(nameField.text) ){
				CreateNewChat();
			}
		} );
	}

	void DeleteThisRoom(){
		ApiChat.main.StartCoroutine(ApiChat.main.DELETEraw( deletes => {
			if( deletes ){
				SetLikeNewView();
				nameField.text = "";
			}else{
			}
		} ,PCI.CHATS,room.ChatId.ToString() ));
	}

	void CreateNewChat(){
		string json = JsonUtility.ToJson (new ChatsCreator( ChatController.main.myUserIDchat, nameField.text));
		print("ESTO ES: "+json);
		ApiChat.main.StartCoroutine(ApiChat.main.POST<RoomsList>( newRoom => {
			if(newRoom == null){
				ChatController.main.SetWarningMessage("Experimentamos problemas para crear "+nameField+" (6)");
			}else{
				SetLikeEditView(newRoom.list[0]);
			}
		} ,PCI.CHATS,json ));
	}



	void PutMembersInRoom(ChatsMembersList chatmbs){
		foreach (ChatsMembers cm in chatmbs.list) {
			if(cm.Members.MemberId != ChatController.main.myUserIDchat ){
				InstantiateCard<IChatMember>(cardMemberPrefab   ,cm );
			}
		}
	}


	void PutInvitesInRoom(InvitationsList invites){
		foreach (Invitations iv in invites.list) {
			if(iv.Members.MemberId != ChatController.main.myUserIDchat ){
				InstantiateCard<IChatMember>(cardInvitedPrefab   ,iv );
			}
		}
	}


	void InstantiateCard<T>(CardMember _c, T _t){
        CardMember card = Instantiate(_c ) ;
        card.transform.SetParent (_c.transform.parent, false);
        cards.Add(card);
        card.gameObject.SetActive(true);
        card.SetValues(_t );
	}

	public void DisposeView(){
		foreach (CardMember c in cards) {
			Destroy(c.gameObject,0.03f);
		}
		cards.Clear();
		this.gameObject.SetActive(false);
		setName.onClick.RemoveAllListeners();
		buttonDelete.onClick.RemoveAllListeners();
		buttonInvite.onClick.RemoveAllListeners();

		nameField.text = "";
	}
}
