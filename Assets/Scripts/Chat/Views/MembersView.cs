﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MembersView : MonoBehaviour,IChatView {

	public CardViewUserNC cardMember;
	public New_EditChatView viewEdit;
	public Button inviteButton;

	public void SetupView(){
		this.gameObject.SetActive(true);
		RegisterListenners();
	}

	void RegisterListenners(){

		inviteButton.onClick.AddListener(()=>{
			GetHisChatIdInServer();
		});	
	}


	void GetHisChatIdInServer(){
		ApiChat.main.StartCoroutine(ApiChat.main.GetChatIdInServer( _id => {
			print("con ProcessFromGetIdInServer: "+_id);
			if(_id > 0){
				SendInvitation(_id);
			}else{
				ChatController.main.SetWarningMessage("Experimentamos problemas para enviar tu invitación.(10)");
			}
		} , cardMember.idUser ));
	}

	void SendInvitation( int _id){
		string json = JsonUtility.ToJson (new InvitationsCreator(  viewEdit.room.ChatId , _id  ));
		ApiChat.main.StartCoroutine(ApiChat.main.POST<InvitationsList>( invite => {
			if(invite == null){
				ChatController.main.SetWarningMessage("Experimentamos problemas para enviar tu invitación.(11)");
			}else{
				cardMember.gameObject.SetActive(false);
				ChatController.main.ActiveView(VIEW_CHAT.EDIT);
				viewEdit.SetLikeEditView(viewEdit.room);
			}
		} ,PCI.INVITATIONS,json ));
	}

	public void DisposeView(){
		this.gameObject.SetActive(false);
		inviteButton.onClick.RemoveAllListeners();
	}

}
