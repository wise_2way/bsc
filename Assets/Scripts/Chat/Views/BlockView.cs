﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class BlockView : MonoBehaviour,IChatView {
	public CardBlocks cardPrefab;
	List<CardBlocks> cards = new List<CardBlocks>();

	// Use this for initialization
	public void SetupView () {
		this.gameObject.SetActive(true);
		CLEARView();
		ShowMyBlockeds();
	}

	void ShowMyBlockeds(){
		Parameter p1 = new Parameter("MemberId",ChatController.main.myUserIDchat);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);

		StartCoroutine(ApiChat.main.GET<BlockList>( blocks  => {
			PutMyBlockeds(blocks);
		} ,PCI.BLOCKS,parameters));
	}


	void PutMyBlockeds(BlockList blocks)
	{
		foreach (Block _blocks in blocks.list) 
		{
			print(_blocks.BlockedMemberId); print(_blocks.BlockedMembers.DisplayName);
			print(_blocks.MemberId); print(_blocks.Members.DisplayName);//Revisar

			InstantiateCard(cardPrefab, _blocks.BlockedMembers.DisplayName, _blocks.MemberId);
		}
	}

	void CLEARView(){
		foreach (CardBlocks c in cards) {
			Destroy(c.gameObject,0.03f);
		}
		cards.Clear();
	}

	void InstantiateCard(CardBlocks _c, string _nameBMb, int _mid){
        CardBlocks card = Instantiate(_c) ;
        card.transform.SetParent (_c.transform.parent, false);
        cards.Add(card);
        card.gameObject.SetActive(true);
        card.SetValues(_nameBMb, _mid);
	}

	public void DisposeView(){
		this.gameObject.SetActive(false);
	}
}
