﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CardInvitation : MonoBehaviour {
	public Text nameMemberRemote;
	public Text nameChat;
	public Button buttonView;
	public ManageInvitationPopUp invP;
	public Text nameRemoteInPopUp;
	public Text nameChatInPopUp;
	public InvitationsView view;
	Invitations inv;


	// Use this for initialization
	public void SetValues (Invitations _inv, string _nameChat) {
		inv = _inv;
		nameMemberRemote.text = _nameChat;
		nameChat.text = _inv.Chats.DisplayName;
		RegisterListener();

		// idChat = _id;
	}

	void DeleteInvitation(){
		ApiChat.main.StartCoroutine(ApiChat.main.DELETEraw( deletes => {
			if( deletes ){
				view.Reload();
				//Recargar pantalla de lista
			}else{
			}
		} ,PCI.INVITATIONS, inv.InvitationId.ToString() ));
	}

	void LoginToChat(){
		string json = JsonUtility.ToJson (new ChatsMembersCreator( inv.Chats.ChatId , ChatController.main.myUserIDchat));
		ApiChat.main.StartCoroutine(ApiChat.main.POST<ChatsMembersList>( cmThisChat => {
			if(cmThisChat == null){
				ChatController.main.SetWarningMessage("Experimentamos problemas para registrarte en el Muro Amarillo.(12)");
			}else{
		      	DeleteInvitation();
			}
		} ,PCI.CHATS_MEMBERS,json ));
	}

	void RegisterListener(){
		buttonView.onClick.AddListener(() =>{ 
			nameRemoteInPopUp.text = nameMemberRemote.text;
			nameChatInPopUp.text =" Te ha invitado a participar en el \n chat <b>"+nameChat.text+"</b>";
			invP.gameObject.SetActive(true);
			StartCoroutine(invP.AskToUser(result =>{
				switch (result) {
					    case "Accept":
					    	LoginToChat();
					      break;
					    case "Reject":
					      DeleteInvitation();
					      break;
					    case "Block":
					      // Bloquea al usuario
					      // DeleteInvitation();
					      break;
					    case "Close":
					      break;
					}	

				invP.gameObject.SetActive(false);
			} ));
		});
	}

}
