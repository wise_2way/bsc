﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CardMember : MonoBehaviour {
	public Text nameMember;
	public Button buttonView;
	public Button buttonDelete;
	ChatsMembers chatsMember;
	Invitations invitation;
	// int idMember;

	// Use this for initialization
	public void SetValues <IChatMember>(ChatsMembers _t) {
		// chatsMember = _cm;
		// if(typeof(_t) == typeof(ChatsMembers)){
			chatsMember = _t as ChatsMembers;
		// }else if(typeof(_t) == typeof(Invitations) ){
			// invitation = _t as Invitations;
		// }

		nameMember.text = _t.Members.DisplayName;

		if( ChatController.main.myUserIDchat == (_t as ChatsMembers).Chats.Owner  ){
			RegisterDeleteListener();
		}else{
			buttonDelete.gameObject.SetActive(false);
		}
	}

	void RegisterDeleteListener(){
		buttonDelete.onClick.AddListener(()=> { 
			ChatController.main.cp.gameObject.SetActive(true);
			ApiChat.main.StartCoroutine(ChatController.main.cp.AskToUser(result =>{
				if(result == "ok"){
					DeleteThisMember();
					print ("ELIGIO OK");
				}else{
					print ("ELIGIO "+result);
				}	
				ChatController.main.cp.gameObject.SetActive(false);
			},"¿Desea eliminar a "+nameMember.text+" de este grupo?" ));
		});
	}

	void DeleteThisMember(){
		Parameter p1 = new Parameter("ChatId",chatsMember.Chats.ChatId);
		Parameter p2 = new Parameter("MemberId",chatsMember.Members.MemberId);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);
		parameters.Add(p2);

		ApiChat.main.StartCoroutine(ApiChat.main.DELETE( deletes => {
			if( deletes ){
				this.gameObject.SetActive(false);
			}else{
			}
		} ,PCI.CHATS_MEMBERS,parameters ));
	}
	

}
