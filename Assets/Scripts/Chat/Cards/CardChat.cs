﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CardChat : MonoBehaviour {
	public Text txtInvisible;
	public Text txtVisible;

    public Text userName;//Eduardo
    
	// Use this for initialization
	public void SetTextMessage (string _msg)
	{
		txtInvisible.text = _msg;
		txtVisible.text = _msg;
	}
	

}
