﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Messages  {

	public ChatsMembers Receivers;
	public int MessageId;
	public int Sender;
	public int Receiver;
	public string Text;
	public int MessageStateId;
	public string CreatedDate; //REVISAR
	public bool IsFile;
	// public string SubmitedDate;
	// public static Messages CreateFromJSON(string jsonString) {
	// 	Debug.Log("para Eduardo: "+ jsonString);
	// 	return JsonUtility.FromJson<Messages>(jsonString);
	// }
}


[System.Serializable]
public class MessagesCreator  {
	public ChatsMembers Receivers;
	public int Sender;
	public string Text;
	public bool IsFile;

	public MessagesCreator(ChatsMembers _Receivers, int _Sender, string _Text, bool _IsFile ){
		Receivers = _Receivers;
		Sender = _Sender;
		Text = _Text;
		IsFile = _IsFile;
	}
}


[System.Serializable]
public class MessagesList  {
	public List<Messages> list;
}
