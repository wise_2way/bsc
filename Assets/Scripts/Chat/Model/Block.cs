﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Block  {

	public Member BlockedMembers, Members;
	public int BlockId;
	public int MemberId;
	public int BlockedMemberId;
	public string CreatedDate;
	// public string SubmitedDate;
	public static Block CreateFromJSON(string jsonString) {
		return JsonUtility.FromJson<Block>(jsonString);
	}
}

[System.Serializable]
public class BlockList  {
	public List<Block> list;
}