using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JsonHelper
{
    public static MessagesListAj getJsonArray(string json)
    {
        Debug.Log(" json llega : "+json);
        // string newJson = "{ \"messagesList\": " + json + "}";
        // Debug.Log("new json: "+newJson);

        MessagesListAj wrapper = JsonUtility.FromJson<MessagesListAj> (json);
        Debug.Log("longitud: "+wrapper.array.Count);

        return wrapper;
    }
 
}
[System.Serializable]
public  class MessagesListAj {
    public List <Messages> array;
}