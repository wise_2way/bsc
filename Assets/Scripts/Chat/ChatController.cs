﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
/*GENERAL_*/
public enum VIEW_CHAT : int{ ROOM =0,LISTS =1, EDIT=2, INVITES =3 , MEMBERS = 4 ,BLOCKS = 5,  SEARCH_MEM = 6 };

public class ChatController :  Singleton<ChatController> {

	public GameObject popUpWarning;
	public IChatView actualView { get; private set;}
	public  int myUserIDchat   { get; private set;}
	public Rooms muroAmarillo { get; private set;}
	public 	Rooms actualRoom { get; private set;}
	public 	Member me { get; private set;}
	public ConfirmationPopUp cp;
	public int tries  =  0;
	public UnityEvent onOpenMuroAmarillo;
	List<IChatView>  views = new List<IChatView>();
	int actualIdView ;
	int previousIdView ;

	public override void MyAwake(){
		this.gameObject.SetActive(false);
	}

	public void SetMyIDChat(int _id){
		myUserIDchat = _id;
	}

	public void SetMyMember(Member _m){
		me = _m;
	}

	public void SetMuroAmarillo(Rooms _r){
		muroAmarillo = _r;
	}

	public void OpenInMuroAmarilloFromMenu(){
		tries =0;
		LoadViews();
		if(DataApp.main.IsRegistered() ){
			LoginController.main.InitLogin();
		}else{
			Debug.LogError("POR FAVOR LANZAR REGISTRO!!!");
		}
	}

	public void CreateUserDUMMY(string _id){
		DataApp.main.SetMyID(_id);
	}

	public void SetChatRoom(Rooms _r){
		print("con SetChatRoom"+_r);
		actualRoom =_r;
	}

	public void OpenMuroAmarillo(){
		actualRoom = muroAmarillo;
		tries = 0;
		onOpenMuroAmarillo.Invoke();
		ActiveView(VIEW_CHAT.ROOM);
		InternetManager.main.loading.SetActive(false);
	}

	public void ActiveView(int _v){
		ActiveView((VIEW_CHAT) _v );
	}

	public void ActiveView(VIEW_CHAT _vc){
		if(actualView != null){
			actualView.DisposeView();
		}
		previousIdView = actualIdView;
		actualIdView = (int)_vc;
		actualView = views[actualIdView];
		actualView.SetupView();
		print("..............Se pide a  "+actualIdView+" y se viene de "+previousIdView);
	}

	public void ReturnToPreviousView(){
		print(".............retorno a "+previousIdView);
		ActiveView(previousIdView);
	}

	public void SetLikeEdit(Rooms _r){
		New_EditChatView mView = (New_EditChatView) views[(int)VIEW_CHAT.EDIT];
		mView.SetLikeEditView(_r);
	}


	void LoadViews(){
		for(int i=0; i<this.transform.childCount; i++){
			views.Add( this.transform.GetChild(i).gameObject.GetComponent<IChatView>() );
			print("//////// AGREGADO: "+this.transform.GetChild(i).gameObject.name+ "  CON ID: "+i );
		}
	}

	public void SetWarningMessage(string _msj){
		Debug.LogWarning(" intento fallido "+tries);
		if(tries<2){
			InternetManager.main.loading.SetActive(true);
			Invoke("InitLogin",0.8f);
			tries++;
		}else if(!popUpWarning.activeSelf) {
			popUpWarning.SetActive(true);
			InternetManager.main.loading.SetActive(false);
			popUpWarning.GetComponentInChildren<Text>().text = _msj;
			Debug.Log(" FALLA ");
		}
	}
	
}
